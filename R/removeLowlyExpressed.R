removeLowlyExpressed = function(ExprSet){
  ## Usuniecie 5% sond o najmniejszej i największej średniej ekspresji
  
  expr_sort = sort(rowMeans(exprs(ExprSet)),index.return=T)
  feat_num= dim(ExprSet)[1] # ilość zestawów sond na macierzy po reannotacji
  cutoff =round(dim(ExprSet)[1]*0.025) # ilość sond do usunięcia po obu stronach
  ind_clear=expr_sort$ix[c(1:cutoff,(feat_num-cutoff):feat_num)]
  newExprSet=ExprSet[-ind_clear,] 
  
  return (newExprSet)
}
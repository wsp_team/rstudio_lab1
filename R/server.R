library(shiny)
library(zip)
library(png)
library(survival)
library(DT)

## Load Data
home = getwd()
load('../EXSET.RData')
message("load")

## Sources
source(paste("..","R", 'PCA.R', sep = "/"))
source(paste("..","R", 'classification.R', sep = "/"))
source(paste("..","R", 'kmeans_ExprSet.R', sep = "/"))
source(paste("..","R", 'hierarchical_clustering.R', sep = "/"))
source(paste("..","R", 'removeLowlyExpressed.R', sep = "/"))
source(paste("..","R", 'genes_Id.R', sep = "/"))
source(paste("..","R", 'cut_genes.R', sep = "/"))
source(paste("..","R", 'summary.R', sep = "/"))
## End of sources

shinyServer(function(input, output) {
  
  
  ### K-MEANS
  k_means_data <- eventReactive(input$kmeans_update,
                                {kmeans_ExprSet(ExprSet, input$kmeans, input$kmeans_nof, input$kmeans_method)})
  
  output$kmeans <- renderText (
    {
      paste("K-means clustering with ", input$kmeans_nof, " features and ", input$kmeans, " clusters:")
    }
    
  )
  output$kmeans_plot <- renderPlot(
    {
      message("kmeans plot")
      k_means_data()
    }
  )
  
  ### Classification
  classification_data <- eventReactive(input$classification_update,
                                       {
                                         clf_ExprSet(ExprSet, input$classification_method)
                                       })
  
  output$CM_plot <- renderPlot(
    {
      message("confusion matrix")
      classification_data()
    }
  )
  
  output$classification_out <- DT::renderDataTable(
    {
      message("input$classification_method")
      classification_data()
    }
  )
  
  
  ### hierarchical_clustering
  hier_data <- eventReactive(input$hier_update,
                             {
                               hierarchical_clustering(ExprSet, input$hier_in)
                             })
  output$hier_out <- renderPlot(
    {
      message("hier_data")
      hier_data()
    }
  )
  
  ### PCA
  pca_data <- eventReactive(input$pca_update,
                            {
                              PCA(ExprSet, input$PCA_plot_type)
                            })
  message("pca1")
  output$pca_out <- renderPlot(
    {
      pca_data()
    }
    
  )
  ## end of pca
  
  ### Cut Genes
  genes_data <- eventReactive(input$cut_update,
                              {
                                cut_genes(ExprSet, input$cut_name, input$type_of_change, 
                                          input$change, input$type_of_regulation)
                              })
  message("cut_genes")
  output$cut_genes_out <- DT::renderDataTable(
    {
      genes_data()
    }
    
  )
  ## end of cyt genes
  
  ### Summary
  sum_data <- eventReactive(input$sum_update,
                            {
                              summary(ExprSet, method, sort_criterion=15, col_nr=5, sep=',')
                            })
  
  output$sum_out <- renderTable({
    sum_data
  })
  
})
